# My Movies

## Description
My Movies is a mobile app based on the Movie DB API. It allows you to follow the last cinema releases and the upcoming ones in the near future in order to create a list of anticipated movies and have an history of the movies you've seen.

## How to install
	git clone https://github.com/Rouksana/movie-app
	npm install 
	ionic serve
	ionic platform add ios
	ionic run ios


