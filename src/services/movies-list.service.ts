import { Injectable } from '@angular/core';
import { AngularFire, FirebaseObjectObservable } from 'angularfire2';

import { AuthService } from './auth.service';

import { StateLike, StateSee, MovieUser } from '../class';


@Injectable()
export class MoviesListService {
    private apiMovies = 'moviesUser/';

    private moviesSeen: string[];
    private moviesToSee: string[];

    constructor(
        private af: AngularFire,
        private authService: AuthService
    ) {
        this.moviesSeen = [];
        this.moviesToSee = [];
    }

    addOrUpdateMovie(idMovie: string, title: string, poster: string, release_date: string, runtime: number, enumState: StateSee) {
        let moviesFirebase = this.af.database.object(this.apiMovies + this.authService.userAuthInfo.uid + '/' + idMovie);

        let d = new Date();
        let date = this.formatDate(d);
        let month = ("00" + (d.getMonth() + 1)).slice(-2);
        let year = d.getFullYear();

        let movieUser = new MovieUser();
        movieUser.state = enumState;
        movieUser.title = title;
        movieUser.poster = poster;
        movieUser.date = date;
        movieUser.runtime = runtime;
        movieUser.month = month;
        movieUser.year = year;
        movieUser.release_date = release_date;

        moviesFirebase.set(movieUser);
    }

    likeMovie(idMovie: string, like: StateLike) {
        let moviesFirebase = this.af.database.object(this.apiMovies + this.authService.userAuthInfo.uid + '/' + idMovie + '/like');
        moviesFirebase.set(like);
    }

    removeMovie(idMovie: string){
        let moviesFirebase = this.af.database.object(this.apiMovies + this.authService.userAuthInfo.uid + '/' + idMovie);
        moviesFirebase.remove();
    }

    getUserMovieData(idMovie: number, data: string){
        return this.af.database.object(this.apiMovies + this.authService.userAuthInfo.uid + '/' + idMovie + '/' + data);
    }

    getMoviesUser(): FirebaseObjectObservable<MovieUser> {
        return this.af.database.object(this.apiMovies + this.authService.userAuthInfo.uid);
    }

    private formatDate(d: Date){
        return ("00" + d.getDate()).slice(-2) + "/" + ("00" + (d.getMonth() + 1)).slice(-2) + "/" + d.getFullYear();
    }
}
