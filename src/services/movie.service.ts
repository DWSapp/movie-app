import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Team } from '../class';

@Injectable()

export class MovieService {
	private apiUrl = 'https://api.themoviedb.org/3/';
	private apiKey = '?api_key=3412dba46cc5061134bb6d95a41ac242';
	private apiFr  = '&language=fr-FR&region=FR';

    constructor (
        private http: Http
    ) {}

    getMovieById(id){
        return this.http.get(this.apiUrl+'movie/'+id+this.apiKey+this.apiFr+'&append_to_response=releases')
        .toPromise()
        .then(data => data.json());
    }

    getMoviesUpcoming(){
	    return this.http.get(this.apiUrl+'movie/upcoming'+this.apiKey+this.apiFr)
        .toPromise()
        .then(this.getData);
    }

    getMoviesCurrent(){
        return this.http.get(this.apiUrl+'movie/now_playing'+this.apiKey+this.apiFr)
        .toPromise()
	    .then(this.getData);
    }

    getMovieSearch(data: string){
        return this.http.get(this.apiUrl+'search/movie'+this.apiKey+this.apiFr+'&query='+data)
        .toPromise()
        .then(this.getData);
    }

    getCredit (film_id: number) {
        return this.http.get(this.apiUrl+'movie/'+film_id+'/credits'+this.apiKey+this.apiFr)
	    .toPromise()
	    .then(this.getDataCredit);
    }

    getVideos (film_id: number) {
        return this.http.get(this.apiUrl+'movie/'+film_id+'/videos'+this.apiKey+this.apiFr+'&append_to_response=videos')
	    .toPromise()
	    .then((res) => {
            return res.json();
        });
    }

    private getData(res: Response) {
        let data = res.json();
        let result = data.results.filter((movie)=> movie.adult == false).filter((movie)=> movie.poster_path !== null).filter((movie)=> movie.overview != '').filter((movie)=> movie.release_date != '');
        return result;
    }

    private getDataCredit(res: Response) {
        let data = res.json();

        let team = new Team();
        team.crew = data.crew;

        let cast = data.cast.filter((actor)=> actor.profile_path !== null);
        team.cast = cast;

        return team;
    }

}
