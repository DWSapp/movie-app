import { Injectable, Inject } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods, FirebaseApp, FirebaseAuth, FirebaseAuthState } from 'angularfire2';

@Injectable()
export class AuthService {
    userAuthInfo: FirebaseAuthState;

    constructor(
        private af: AngularFire, 
        @Inject(FirebaseAuth) private auth: FirebaseAuth,
        @Inject(FirebaseApp) private firebase: any
    ) { 
        this.af.auth.subscribe(auth => {
            this.userAuthInfo = auth;
        });
    }

    login(email: string, password: string) {
        let creds = { email: email, password: password };
        return this.af.auth.login(creds, {
            method: AuthMethods.Password,
            provider: AuthProviders.Password
        });
    }

    logout() {
        return this.af.auth.logout();
    }

    forgot(email: string) {
        return this.firebase.auth().sendPasswordResetEmail(email);
    }

    register(email: string, password: string) {
        return this.af.auth.createUser({
            email: email,
            password: password
        });
    }
}