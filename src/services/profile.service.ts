import { Injectable } from '@angular/core';
import { AngularFire } from 'angularfire2';

import { UserProfile } from '../class';
import { AuthService } from './auth.service';

@Injectable()
export class ProfileService {
    private apiUsers = 'users/';

    constructor(
        private af: AngularFire,
        private authService: AuthService
    ) {}

    getProfile(uid: string) {
        return this.af.database.object(this.apiUsers + uid);
    }

    updateProfile(uid: string, email: string, name: string) {
        let user = this.af.database.object(this.apiUsers + uid);

        let userProfile = new UserProfile();
        userProfile.name = name;
        userProfile.email = email;

        return user.set(userProfile);
    }
}
