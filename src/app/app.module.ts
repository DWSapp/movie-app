import { NgModule, ErrorHandler, LOCALE_ID } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SearchPage } from '../pages/search/search';
import { LoginPage } from '../pages/login/login';
import { ProfilPage } from '../pages/profil/profil';
import { MoviePage } from '../pages/movie/movie';
import { PasswordPage } from '../pages/password/password';
import { RegisterPage } from '../pages/register/register';
import { TabsPage } from '../pages/tabs/tabs';

import { AuthService, MovieService, MoviesListService, ProfileService } from '../services';

import { AngularFireModule } from 'angularfire2';

export const firebaseConfig = {
    apiKey: "AIzaSyBQYDjhi4NhMRQazeIF2PMQuBSdyyTZEks",
    authDomain: "movie-app-268ff.firebaseapp.com",
    databaseURL: "https://movie-app-268ff.firebaseio.com",
    storageBucket: "movie-app-268ff.appspot.com",
    messagingSenderId: "318914432254"
};

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        SearchPage,
        ProfilPage,
        LoginPage,
        MoviePage,
        PasswordPage,
        RegisterPage,
        TabsPage
    ],
    imports: [
        IonicModule.forRoot(MyApp, {
            backButtonText: "Retour",
        }),
        AngularFireModule.initializeApp(firebaseConfig),
        ReactiveFormsModule,
        BrowserModule,
        HttpModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        SearchPage,
        ProfilPage,
        LoginPage,
        MoviePage,
        PasswordPage,
        RegisterPage,
        TabsPage
    ],
      providers: [
          {provide: ErrorHandler, useClass: IonicErrorHandler},
          { provide: LOCALE_ID, useValue:"fr-FR" },
          FormBuilder,
          AuthService,
          MovieService,
          MoviesListService,
          ProfileService
          ]
})
export class AppModule {}
