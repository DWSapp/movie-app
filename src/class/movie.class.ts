export class Movie {
    constructor(
        public id: number,
        public release_date: string,
        public poster_path: string,
        public title: string,
        public overview: string,
        public runtime: number
    ) {}
}

export enum StateSee {
    ToSee = 1,
    Seen
}

export enum StateLike {
    Null,
    Like,
    Dislike
}

export class MovieUser {
    $exists?: boolean;
    $key?: string;
    $value: string;
    state: StateSee;
    like: StateLike;
    title: string;
    poster: string;
    date: string;
    runtime: number;
    release_date: string;
    month: string;
    year: number;
}
