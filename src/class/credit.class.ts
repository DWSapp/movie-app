export interface Cast {
    cast_id: number;
    character: string;
    credit_id: string;
    id: number;
    name: string;
    order: number;
    profile_path: string;
}

export interface Crew {
    credit_id: string;
    department: string;
    id: number;
    job: string;
    name: string;
    profile_path: string;
}

export interface Credit {
    id: number;
    cast: Cast[];
    crew: Crew[];
}

export class Team {
    crew: Crew[];
    cast: Cast[];
}