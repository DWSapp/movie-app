import { Component, Injectable } from '@angular/core';
import { NavController } from 'ionic-angular';

import { PasswordPage } from '../password/password';
import { RegisterPage } from '../register/register';
import { ProfilPage } from '../profil/profil';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFire, FirebaseAuthState } from 'angularfire2';

import { AuthService, ProfileService } from '../../services';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
@Injectable()
export class LoginPage {
    loginForm: FormGroup;
    message: string;
    authState: FirebaseAuthState;

    constructor(
        public navCtrl: NavController,
        private af: AngularFire,
        fb: FormBuilder,
        private authService: AuthService,
        private profileService: ProfileService
    ) {
        this.authState = null;

        this.af.auth.subscribe(auth => {
            this.authState = auth;
            if (auth !== null) {
                this.authService.userAuthInfo = auth;
            }
        });

        this.loginForm = fb.group({
            email: fb.control('', [Validators.required, Validators.minLength(4)]),
            password: fb.control('', [Validators.required, Validators.minLength(6)])
        });
    }

    login() {
        this.message = "";
        if (this.loginForm.value.email == '' || this.loginForm.value.password == '') {
            this.message = "Veuillez remplir tous les champs";
            return;
        }

        this.authService.login(this.loginForm.value.email, this.loginForm.value.password)
            .then(() => {
                this.navCtrl.setRoot(ProfilPage);
            },
            (err) => {
                console.log(err);
                if (err['code'] == "auth/user-not-found") {
                    this.message = "User inconnu";
                } else if (err['code'] == "auth/invalid-email") {
                    this.message = "Email invalide"
                } else if (err['code'] == "auth/operation-not-allowed") {
                    this.message = "Error"
                } else if (err['code'] == "auth/wrong-password") {
                    this.message = "Mauvais mot de passe"
                }
            });
    }

    clickPassword(event) {
        this.navCtrl.push(PasswordPage);
    }

    clickRegister(event) {
        this.navCtrl.push(RegisterPage);
    }
}

