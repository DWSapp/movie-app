import { Component, Injectable, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthService } from '../../services';

@Component({
    selector: 'page-password',
    templateUrl: 'password.html'
})
@Injectable()
export class PasswordPage implements OnInit {
    passwordForm: FormGroup;
    message : string;

    constructor(
        public navCtrl: NavController, 
        fb: FormBuilder,
        private authService: AuthService
    ) {
        this.passwordForm = fb.group({
            email:fb.control('', [Validators.required, Validators.minLength(4)])
        });

    }

    ngOnInit() {
        this.message = null;
    }

    getMessage() {
        return this.message;
    }

    password(){
        this.message = "";
        if (this.passwordForm.value.email == ''){
            this.message = "Veuillez remplir tous les champs";
            return;
        }

        this.authService.forgot(this.passwordForm.value.email)
        .then(
            (success) => {
                console.log(success);
            }, (err) => {
                console.log(err);
            }
        )
        this.message = 'Un mail a été envoyé à cette adresse, si celle-ci est valide';
    }
}
