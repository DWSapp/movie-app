import { Component, Injectable } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ProfilPage } from '../profil/profil';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFire, FirebaseAuthState } from 'angularfire2';

import { AuthService, ProfileService } from '../../services';

@Component({
    selector: 'page-register',
    templateUrl: 'register.html'
})
@Injectable()
export class RegisterPage {
    registerForm: FormGroup;
    message: string;
    authState: FirebaseAuthState;

    constructor(
        public navCtrl: NavController,
        private af: AngularFire,
        fb: FormBuilder,
        private authService: AuthService,
        private profileService: ProfileService
    ) {
        this.authState = null;

        this.af.auth.subscribe(auth => {
            this.authState = auth;
            if (auth !== null) {
                this.authService.userAuthInfo = auth;
            }
        });

        this.registerForm = fb.group({
            name: fb.control('', [Validators.required, Validators.minLength(4)]),
            email: fb.control('', [Validators.required, Validators.minLength(4)]),
            password: fb.control('', [Validators.required, Validators.minLength(6)])
        });
    }

    register() {
        this.message = "";

        if (this.registerForm.value.name == '' || this.registerForm.value.email == '' || this.registerForm.value.password == '') {
            this.message = "Veuillez remplir tous les champs";
        } else {
            this.authService.register(this.registerForm.value.email, this.registerForm.value.password)
                .then((success) => {
                    this.profileService.updateProfile(
                        success.uid,
                        this.registerForm.value.email,
                        this.registerForm.value.name
                    )
                        .then(res => {
                            console.log(success);
                            this.navCtrl.setRoot(ProfilPage);
                        })
                }).catch(
                (err) => {
                    console.log(err);
                    if (err['code'] == "auth/invalid-email") {
                        this.message = "Email invalide"
                    } else if (err['code'] == "auth/weak-password") {
                        this.message = "Le mot de passe doit être de 6 caractères minimun"
                    } else if (err['code'] == "auth/email-already-in-use") {
                        this.message = "Email déja utilisé"
                    } else if (err['code'] == "auth/operation-not-allowed") {
                        this.message = "Error"
                    }
                })
        }
    }
}

