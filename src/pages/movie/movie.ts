import { Component, OnInit, Injectable, Inject } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { InAppBrowser } from 'ionic-native';

import { MovieService, MoviesListService } from '../../services';

import { StateSee, StateLike, Movie, Crew, Cast, Team } from '../../class';

import { AngularFire, FirebaseAuth } from 'angularfire2';

@Component({
	selector: 'page-movie',
	templateUrl: 'movie.html'
})
@Injectable()
export class MoviePage implements OnInit {
	selectedMovie: Movie;

	date: string;
	title: string;
	poster: string;
	runtime: number;
	urlVideo: string;

	cast: Cast[];
	directors: Crew[];
	time: string;

	state: string = '';
	like: string = '';

	loader: Loading;
	
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		private af: AngularFire,
		private movieService: MovieService,
		private moviesListService: MoviesListService,
		public loadingCtrl: LoadingController,
		@Inject(FirebaseAuth) public fbauth: FirebaseAuth
	) {
			
	}

	ngOnInit(){
		this.presentLoadingDefault();
		let movieID = this.navParams.get('movie');
		this.getMovie(movieID);
	}

	presentLoadingDefault() {
		this.loader = this.loadingCtrl.create({
			content: 'Chargement...',
		});

		this.loader.present();
	}

	getMovie(selectedMovie: string){
		this.movieService.getMovieById(selectedMovie)
		.then( data => {
			this.selectedMovie = data;

			this.title = data.title;
			this.poster = data.poster_path;
			this.runtime = data.runtime;

			let country_array = data['releases']['countries']
			for (const d in country_array){
				if(country_array[d]['iso_3166_1'] == 'FR'){
					this.date = country_array[d]['release_date'];
				}
				if(this.date == null){
					this.date = data.release_date;
				}
			}

			if(this.selectedMovie.runtime != 0){
				let hours = Math.floor( this.selectedMovie.runtime / 60);
				let minutes = this.selectedMovie.runtime % 60;
				if (minutes < 10) {
					this.time = hours + 'h0' + minutes;
				}else{
					this.time = hours + 'h' + minutes;
				}
			}

			this.af.auth.subscribe(auth => {
				if (auth !== null) {
					this.getUserMovieData();
				}
			});

			let movieCreditPromise = this.getMovieCredit();
			let movieVideoPromise = this.getMovieVideo();

			Promise.all([movieCreditPromise, movieVideoPromise]).then(val => {
				this.loader.dismiss();
			});
		});
	}

	getMovieVideo(){
		return this.movieService.getVideos(this.selectedMovie.id)
		.then((data) => {
			let idsVideo = [];
			let idsVideoVOST = [];

			for(const video of data.results) {
				let name = video['name']
				if(video['site'] === 'YouTube') {
					if( name.includes("VOST")){
						idsVideoVOST.push(video['key']);
					}else{
						idsVideo.push(video['key']);
					}
				}

				if(idsVideoVOST.length > 0){
					this.urlVideo = "https://www.youtube.com/embed/"+idsVideoVOST[idsVideoVOST.length-1]+"?rel=0";
				}else if(idsVideo.length > 0){
					this.urlVideo = "https://www.youtube.com/embed/"+idsVideo[idsVideo.length-1]+"?rel=0";
				}
				
			}
		})
	}

	getMovieCredit(){
		return this.movieService.getCredit(this.selectedMovie.id)
		.then((data: Team) => {

			this.cast = data.cast;
			this.directors = [];

			for(const person of data.crew) {
				if(person['job'] === 'Director') {
					this.directors.push(person);
				}
			}
		})
	}

	getUserMovieData(){
		this.moviesListService.getUserMovieData(this.selectedMovie.id, 'state').subscribe(res => {
			if(res.$value == StateSee.ToSee){
				this.state = 'toSee'
			}else if(res.$value == StateSee.Seen){
				this.state = 'seen'
			}
		});
		this.moviesListService.getUserMovieData(this.selectedMovie.id, 'like').subscribe(res => {
			if(res.$value == StateLike.Like){
				this.like = 'like'
			}else if(res.$value == StateLike.Dislike){
				this.like = 'dislike'
			}else{
				this.like = ''
			}
		});
	}

	clickToSee(id) {
		let movieState = StateSee.ToSee;
		this.moviesListService.addOrUpdateMovie(id, this.title, this.poster, this.date, this.runtime, movieState);
	}

	clickSeen(id, like){
		let movieState = StateSee.Seen;
		this.moviesListService.addOrUpdateMovie(id, this.title, this.poster, this.date, this.runtime, movieState);
	}

	clickLike(id, like){
		this.moviesListService.likeMovie(id, like);
	}

	clickUrl($url){
		new InAppBrowser($url, '_self');
	}
}
