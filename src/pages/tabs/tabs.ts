import { Component } from '@angular/core';

import { AngularFire } from 'angularfire2';
import { AuthService } from '../../services/auth.service';

import { HomePage } from '../home/home';
import { SearchPage } from '../search/search';
import { ProfilPage } from '../profil/profil';
import { LoginPage } from '../login/login';

@Component({
	templateUrl: 'tabs.html'
})
export class TabsPage {
	tab1Root: any = HomePage;
	tab2Root: any = SearchPage;
	tab3Root: any = ProfilPage;

	constructor(
		af: AngularFire,
		private authService: AuthService
	) {

		af.auth.subscribe(auth => {
			this.authService.userAuthInfo = auth;
			if(auth !== null) {
				this.tab3Root = ProfilPage
			}
			else {
				this.tab3Root = LoginPage
			}
		});
	}

}
