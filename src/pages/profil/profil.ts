import { Component, Injectable, OnInit, OnDestroy } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { MoviePage } from '../movie/movie';

import { AngularFire, FirebaseAuthState } from 'angularfire2';

import { AuthService, ProfileService, MovieService, MoviesListService } from '../../services';

import { StateSee, StateLike } from '../../class';

@Component({
    selector: 'page-profil',
    templateUrl: 'profil.html'
})
@Injectable()
export class ProfilPage implements OnInit, OnDestroy {

    tabs: string = "stats";

    // User info
    userData: any;
    userName: string;

    // List movies user
    userMoviesToSee;
    userMoviesSeen;
    countMovieAll: number = 0;

    // Stat movies month
    month: string;
    countMovieMonth: number = 0;
    monthNames = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Decembre"];

    // Stat movies year
    year: number;
    countMovieYear: number = 0;

    // Stat runtime
    runtime: number = 0;
    time: string;

    // Stat movies to see upcoming 
    countMovieToSeeUp: number = 0;
    // Stat movies to see released 
    countMovieToSeeRe: number = 0;

    // Stat movies like
    countLike : number = 0;
    // Stat movies dislike
    countDislike : number = 0;

    listMoviesUser: any;

    constructor(
        public navCtrl: NavController,
        private af: AngularFire,
        private authService: AuthService,
        private profileService: ProfileService,
        private movieService: MovieService,
        private moviesListService: MoviesListService
    ) {
        this.userMoviesToSee = [];
        this.userMoviesSeen = [];
    }

    ngOnInit(){
        this.af.auth.subscribe(auth => {
            if (auth !== null) {
                this.authService.userAuthInfo = auth;
                this.getUserData(auth);
            }
        });
	}

    private getUserData(auth: FirebaseAuthState) {
        this.profileService.getProfile(auth.uid).subscribe(data => {
            this.userData = data;
            this.userName = this.userData.name;
        });

        this.listMoviesUser = this.moviesListService.getMoviesUser().subscribe(res => {
            delete res.$exists;
            delete res.$key;

            this.userMoviesToSee = [];
            this.userMoviesSeen = [];
            this.countMovieAll = 0;
            this.countMovieMonth = 0;
            this.countMovieYear = 0;
            this.countMovieToSeeUp = 0;
            this.countMovieToSeeRe = 0;
            this.countLike = 0;
            this.countDislike = 0;

            let d = new Date();
            let monthNb = ("00" + (d.getMonth() + 1)).slice(-2);
            this.month = this.monthNames[d.getMonth()];
            this.year = d.getFullYear();

            if(!res.$value && res.$value !== null){
                for (const movieId of Object.keys(res)) {

                    this.runtime = 0;

                    // List To See
                    if(res[movieId].state == StateSee.ToSee){
                        let m = res[movieId];
                        m['id'] = movieId;
                        this.userMoviesToSee.push(m);
                        this.userMoviesToSee.sort(function(a,b) { 
                            return new Date(a.release_date).getTime() - new Date(b.release_date).getTime() 
                        });
                        
                        let r = new Date(res[movieId].release_date);
                        if(r > d){
                            this.countMovieToSeeUp++;
                        }else{
                            this.countMovieToSeeRe++;
                        }
                    }

                    // List Seen
                    if(res[movieId].state == StateSee.Seen){
                        let m = res[movieId];
                        m['id'] = movieId;
                        this.userMoviesSeen.push(m);
                        this.userMoviesSeen.sort(function(a,b) { 
                            return new Date(b.release_date).getTime() - new Date(a.release_date).getTime() 
                        });

                        this.countMovieAll++

                        // Stat movie month
                        if(res[movieId].month == monthNb){ this.countMovieMonth++; }

                        // Stat movie year
                        if(res[movieId].year == this.year){ this.countMovieYear++; }

                        // Stat movie like
                        if(res[movieId].like == StateLike.Like){ this.countLike++; }
                        else if(res[movieId].like == StateLike.Dislike){ this.countDislike++; }
                    }

                    // Stat runtime 
                    for(const m of this.userMoviesSeen){
                        this.runtime += m.runtime;
                        let hours = Math.floor( this.runtime / 60);
                        let minutes = this.runtime % 60;
                        if (minutes < 10) {
                            this.time = hours + 'h0' + minutes;
                        }else{
                            this.time = hours + 'h' + minutes;
                        }
                    }
                }
            }else{
                this.userMoviesToSee = null;
                this.userMoviesSeen = null;
            }
       });

    }

    ngOnDestroy() {
        this.listMoviesUser.unsubscribe();
    }

    clickMovie(movieId) {
        this.navCtrl.push(MoviePage, {
            movie: movieId
        });
    }

    removeMovie(id){
        this.moviesListService.removeMovie(id);
    }

    logout() {
        this.authService.logout().then(
            (success) => {
                this.navCtrl.setRoot(LoginPage);
            }
        );
    }
}

