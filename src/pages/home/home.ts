import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';

import { MoviePage } from '../movie/movie';
import { Movie } from '../../class/movie.class';
import { MovieService } from '../../services';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage implements OnInit {
    movies : Array<Movie>;
    movieFocus: number;
    loader: Loading;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private movieService: MovieService,
        public loadingCtrl: LoadingController
    ) {}

    ngOnInit(){
        this.presentLoadingDefault();
        this.movies = [];
        this.movieFocus = null;
        this.getMovies();
    }

    presentLoadingDefault() {
		this.loader = this.loadingCtrl.create({
			content: 'Chargement...',
		});

		this.loader.present();
	}

    getMovies(){
        let p1 = this.movieService.getMoviesCurrent()
        .then( dataC => {
            this.movies = this.sortMovies(this.movies.concat(dataC));
            this.setFocus();
        });

        let p2 = this.movieService.getMoviesUpcoming()
        .then( dataU => {
            this.movies = this.sortMovies(this.movies.concat(dataU));
            this.setFocus();
        });

        Promise.all([p1, p2]).then(values => {
            this.loader.dismiss();
        });
    }

    sortMovies(movies: Movie[]) {
        let moviesFiltered = movies.sort((a, b) => {
            if(new Date(a.release_date) < new Date(b.release_date)){
                return -1;
            }
            if(new Date(a.release_date) > new Date(b.release_date)){
                return 1;
            }
            return 0;
        })

        let moviesFilteredUnique = [];
        let ids = [];

        for(const i of moviesFiltered) {
            if(ids.indexOf(i.id) === -1) {
                ids.push(i.id);
                moviesFilteredUnique.push(i);
            }
        }

        return moviesFilteredUnique;
    }

    setFocus() {
        let moviesFiltered = this.movies.filter(m => new Date(m.release_date) >= new Date());
        this.movieFocus = moviesFiltered[0] ? moviesFiltered[0].id : null;

        setTimeout(() => {
            try {
                document.getElementById('focus').scrollIntoView();
            }
            catch(e) {
                console.warn("element focus not found")
            }
        }, 200);
    }

    clickMovie(movieId) {
        this.navCtrl.push(MoviePage, {
            movie: movieId
        });
    }

}
