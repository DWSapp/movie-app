import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Keyboard } from 'ionic-native';

import { MoviePage } from '../movie/movie';
import { MovieService } from '../../services'

@Component({
    selector: 'page-search',
    templateUrl: 'search.html',
    providers: [MovieService]
})
export class SearchPage {

    moviesSerach: MovieService;
    test: string = null;

    constructor(
        public navCtrl: NavController,
        private movieService: MovieService
    ) {}

    getMovieSearch(event: KeyboardEvent) {
        let term = (<any>event.target).value;

        if(term != ''){
            this.movieService.getMovieSearch(term)
            .then( data => this.moviesSerach = data.sort((a, b) => {
                if(new Date(a.release_date) > new Date(b.release_date)){
                    return -1;
                }else if(new Date(a.release_date) < new Date(b.release_date)){
                    return 1;
                } else {
                    return 0;
                }
            }));
        } else {
            this.moviesSerach = null;
        }
    }

    clickMovie(movie) {
        this.navCtrl.push(MoviePage, {
            movie: movie
        });
    }

    validSearch(){
        Keyboard.close();
    }
}
